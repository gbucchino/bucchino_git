#!/usr/bin/python

"""
	Tool for DNS benchmarking
	Author: Geoffrey Bucchino
	Repo: https://gitlab.com/gbucchino/bucchino_git/blob/master/dns_benchmarking/main.py
"""

import dns.resolver # pip install dnspython
import dns.reversename
from time import process_time
import random
import string
from random import randint
from argparse import ArgumentParser

RR = ('A', 'AAAA', 'MX', 'TXT', 'PTR')
COEF = 1000
LOOP = 10

def getDomains():
	""" Return the list of domains per RR """
	d = {}
	domains = [
		'www.duckduckgo.com',
		'www.google.com',
		'www.bucchino.org',
		'www.cisco.com',
		'www.microsoft.com',
		'www.github.com',
		'www.gitlab.com',
		'ietf.org',
		'www.icann.org',
		'www.ripe.net',
		'nlnetlabs.nl',
		'www.nic.cz',
		'wikipedia.org'
	]
	d['A'] = domains
	d['AAAA'] = domains
	d['MX'] = []
	d['TXT'] = []
	d['PTR'] = []
	return d

def getDomainsRandom():
	""" Retun a list of random domains per RR """
	d = {}
	d['A'] = []
	d['AAAA'] = []
	for entry in range(10):
		domain = random_string(randint(5,10))
		tld = random.choice(['com', 'fr', 'org'])
		d['A'].append('www.%s.%s' % (domain, tld))
		d['AAAA'].append('www.%s.%s' % (domain, tld))

	d['MX'] = []
	d['TXT'] = []
	d['PTR'] = []
	return d

def random_string(length):
    """ Generate a random string """
    return "".join(random.choice(string.ascii_uppercase) for _ in range(length)).lower()

def queryDNS(domain, rr, display, ptr=None):
	""" Doing a query DNS to domain for resolv it """
	try:
		answers = dns.resolver.query(domain, rr)
		if isinstance(answers[0], dns.rdtypes.IN.A.A):
			ptr.append(answers[0].to_text())
		if display:
			for answer in answers:
				print(f"IP for domain {domain}: {answer}")
	except dns.resolver.NXDOMAIN:
		if display:
			print(f"Could not resolve DNS {domain}")
		pass
	except dns.resolver.NoAnswer:
		pass

def resolvDNS(fd, domain, rr,  indice, display, ptrs=[]):
	""" Resolv the DNS and calculate the time """
	# First query
	t1_first = process_time()
	queryDNS(domain=domain, rr=rr, display=display, ptr=ptrs)
	t2_first = process_time()

	# Second query
	t1_cached = process_time()
	queryDNS(domain=domain, rr=rr, display=display, ptr=ptrs)
	t2_cached = process_time()

	time1 = (t2_first - t1_first) * COEF
	time_cached = (t2_cached - t1_cached) * COEF

	fd.write("%s %f %f %d\n" % (domain, time1, time_cached, indice))
	print("Request time for %s: %f; time cached: %f" % (
			domain,
			time1 / COEF,
			time_cached / COEF
			)
	)


def resolvPTR(ptr):
	""" Resolve PTR address """
	try:
		return dns.reversename.from_address(str(ptr))
	except dns.resolver.NXDOMAIN:
		pass
	except dns.resolver.NoAnswer:
		pass	

def checkArguments():
	""" Check the arguments passed """
	parser = ArgumentParser(description='DNS benchmark')
	parser.add_argument('-r', '--rr', help='Resource Record', choices=RR)
	parser.add_argument('-a', '--random', help='Get randoms domains', action='store_true')
	parser.add_argument('-d', '--display', help='Display results from DNS query', action='store_true')
	return parser.parse_args()

if __name__ == "__main__":
	args = checkArguments()
	display = False

	if args.rr is None:
		print(f"Please, specify the arguments -r: {RR}")
		exit(1)

	if args.display:
		display = True

	fd = open(f"results_{args.rr}", "w")
	indice = 1

	# Get domains random
	if args.random:
		print("Checking TTL domains random")
		domains = getDomainsRandom()
		
		for domain in domains[args.rr]:
			resolvDNS(fd, domain, args.rr, indice, display)
			indice = indice + 1
	else:
		# Get domains	
		domains = getDomains()
		for domain in domains[args.rr]:
			resolvDNS(fd, domain, args.rr, indice, display, domains['PTR'])
			indice = indice + 1

	# We remove duplicate entry
	domains['PTR'] = list(dict.fromkeys(domains['PTR']))

	if len(domains['PTR']) > 0:
		for ptr in domains['PTR']:
			# Resolve PTR address
			d = resolvPTR(ptr=ptr)
			# Request DNS query
			resolvDNS(fd, d, "PTR", indice, display)
			indice = indice + 1
	fd.close()
